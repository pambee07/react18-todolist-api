// Importer les fonctionnalités de React ainsi que axios
import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]);

  // Utilise le hook useEffect pour effectuer une action au montage du composant
  useEffect(function () {
    // Fonction pour effectuer la requête API
    async function fetchTodos() {
      try {
        // Effectue une requête GET à l'API en utilisant Axios
        const response = await axios.get(API_URL);

        // Met à jour l'état avec les données de l'API
        setTodos(response.data);
      } catch (error) {
        // Gère les erreurs en affichant un message dans la console
        console.error("Erreur lors de la récupération des todos:", error);
      }
    }

    // Appelle la fonction fetchTodos lors du montage du composant
    fetchTodos();
  }, []);

  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
